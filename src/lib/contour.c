/*------------------------------------------------------------------------------
 * contour.c
 *     Polygon Contour functions and utilities. Within the context of this
 *     program, a contour constitutes an analogue of PostgreSQL's POLYGON
 *     type.
 *
 * Copyright (c) 2018, Nead Werx, Inc.
 * Copyright (c) 2018, Chris Autry
 *
 * IDENTIFICATION
 *      contour.c
 *
 *------------------------------------------------------------------------------
 */

#include "contour.h"

/*
 * void contour_bounding_box( struct contour *, Point *, Point * )
 *     Computes the bounding box of a contour by finding the minima and
 *     maxima of both the X and Y coordinates.
 * 
 * Arguments:
 *     contour * c:     Contour of which we are computing the bounding box.
 *     Point * minumum: Pass-by-reference return for the minimum x,y coordinate
 *                      of one corner of the bounding box for contour c.
 *     Point * maximum: Pass-by-reference return for the maximum x,y coordinate
 *                      of one corner of the bounding box for contour c.
 * Return:
 *     Point * min, Point * max (pass-by-reference).
 * Error Conditions:
 *     No result returned when provided a NULL contour
 *     Fatal error upon failure to allocate memory for pass-by-reference result
 */
void contour_bounding_box( struct contour * c, Point * min, Point * max )
{
    unsigned int i     = 0;
    double       max_x = 0;
    double       max_y = 0;
    double       min_x = 0;
    double       min_y = 0;

    if( c == NULL )
    {
        return;
    }

    max_x = -DBL_MAX;
    max_y = -DBL_MAX;
    min_x = DBL_MAX;
    min_y = DBL_MAX;

    for( i = 0; i < c->num_points; i++ )
    {
        if( c->points[i]->x < min_x )
        {
            min_x = c->points[i]->x;
        }

        if( c->points[i]->x > max_x )
        {
            max_x = c->points[i]->x;
        }

        if( c->points[i]->y < min_y )
        {
            min_y = c->points[i]->y;
        }

        if( c->points[i]->y > max_y )
        {
            max_y = c->points[i]->y;
        }
    }

    if( min == NULL )
    {
        min = ( Point * ) palloc0( sizeof( Point ) );
    }

    if( max == NULL )
    {
        max = ( Point * ) palloc0( sizeof( Point ) );
    }

    if( min == NULL || max == NULL )
    {
        ereport(
            ERROR,
            (
                errcode( ERRCODE_OUT_OF_MEMORY ),
                errmsg( "Could not create contour bounding box" )
            )
        );
    }

    min->x = min_x;
    min->y = min_y;
    max->x = max_x;
    max->y = max_y;

    return;
}

/*
 * bool contour_counterclockwise( struct contour * )
 *     Determines the ordering of a contours points. This also sets the _cc bit
 *     of the contour.
 *
 * Arguments: 
 *     struct contour * c: The contour of which we are determining orientation.
 * Return:
 *     bool result: True indicates that the points forming contour c are
 *                  arranged in counterclockwise order. False indicates
 *                  clockwise arrangement.
 * Error Conditions:
 *     Returns false when NULL contour is provided.
 */
bool contour_counterclockwise( struct contour * c )
{
    double       area = 0.0;
    unsigned int i    = 0;

    if( c == NULL )
    {
        return false;
    }

    if( c->_precomputed_cc )
    {
        return c->_cc;
    }

    c->_precomputed_cc = true;

    for( i = 0; i < c->num_points - 1; i++ )
    {
        area += c->points[i]->x * c->points[i+1]->y
              - c->points[i+1]->x * c->points[i]->y;
    }

    area += c->points[c->num_points - 1]->x * c->points[0]->y
          + c->points[0]->x * c->points[c->num_points - 1]->y;

    if( fabs( area ) <= DBL_EPSILON )
    {
        c->_cc = false;
    }
    else
    {
        c->_cc = true;
    }

    return c->_cc;
}

/*
 * bool contour_clockwise( struct contour * )
 *     Inverse operand of contour_counterclockwise.
 *
 * Arguments:
 *     struct contour * c: The contour of which we are determining orientation.
 * Return:
 *     bool result: True indicates that the points forming contour c are arranged
 *                  in clockwise order. False indicates counterclockwise.
 * Error Conditions:
 *     Returns false when NULL contour is provided.
 */
bool contour_clockwise( struct contour * c )
{
    if( c == NULL )
    {
        return false;
    }

    return !contour_counterclockwise( c );
}

/*
 * void contour_set_clockwise( struct contour * )
 *     Reorders points of contour c such that they are in a clockwise
 *     arrangement.
 *
 * Arguments:
 *     struct contour * c: The contour we are reorienting.
 * Return:
 *     None.
 * Error conditions:
 *     No effect when a NULL contour is provided.
 */
void contour_set_clockwise( struct contour * c )
{
    if( c == NULL )
    {
        return;
    }

    if( contour_counterclockwise( c ) )
    {
        contour_change_orientation( c );
    }

    return;
}

/*
 * void contour_set_counterclockwise( struct contour * )
 *     Reorders points of contour c such that they are in a counterclockwise
 *     orientation.
 *
 * Arguments:
 *     struct contour * c: The contour we are reorienting.
 * Return:
 *     None.
 * Error Conditions:
 *     No effect when a NULL contour is provided.
 */
void contour_set_counterclockwise( struct contour * c )
{
    if( c == NULL )
    {
        return;
    }

    if( !( contour_counterclockwise( c ) ) )
    {
        contour_change_orientation( c );
    }

    return;
}

/*
 * void contour_change_orientation( struct contour * )
 *     Reorders the passed contour. This is wrapped by:
 *         contour_set_clockwise()
 *         contour_set_counterclockwise()
 *
 * Arguments:
 *     struct contour * c: The contour we are reorienting.
 * Return:
 *     None.
 * Error Conditions:
 *     No effect when a NULL contour is provided.
 */
void contour_change_orientation( struct contour * c )
{
    unsigned int start_i  = 0;
    unsigned int end_i    = 0;
    unsigned int j        = 0;
    Point *      temp     = NULL;

    if( c == NULL )
    {
        return;
    }

    end_i = c->num_points;

    while( start_i < end_i )
    {
        for( j = 0; j < c->num_holes; j++ )
        {
            if( c->holes[j] == start_i )
            {
                c->holes[j] = end_i;
            }
            else if( c->holes[j] == end_i )
            {
                c->holes[j] = start_i;
            }
        }

        temp               = c->points[start_i];
        c->points[start_i] = c->points[end_i];
        c->points[end_i]   = temp;

        start_i++;
        end_i--;
    }

    c->_cc = !( c->_cc );
    return;
}

/*
 * void contour_erase_point( struct contour *, unsigned int )
 *     Remove the point at the specified index
 *
 * Arguments:
 *     struct contour * c: The contour being modified
 *     unsigned int i:     The index of the point we are removing
 * Return:
 *     None.
 * Error Conditions:
 *     No effect when a NULL contour is provided or the index exceeds
 *      number of points in contour c.
 *     Error on failure to reallocate the point array.
 *     Error on failure to recompute holes.
 */
void contour_erase_point( struct contour * c, unsigned int i )
{
    unsigned int   j           = 0;
    unsigned int   h           = 0;
    unsigned int   c_i         = 0;
    unsigned int * temp_holes  = NULL;
    Point **       temp_points = NULL;

    if( c == NULL )
    {
        return;
    }

    if( i >= c->num_points )
    {
        return;
    }

    pfree( c->points[i] );
    c->points[i] = NULL;

    temp_points = ( Point ** ) palloc0(
        sizeof( Point * ) * ( c->num_points - 1 )
    );

    if( temp_points == NULL )
    {
        ereport(
            ERROR,
            (
                errcode( ERRCODE_OUT_OF_MEMORY ),
                errmsg( "Could not resize contour point array" )
            )
        );
    }

    for( j = 0; j < c->num_points; j++ )
    {
        if( c->points[j] != NULL )
        {
            temp_points[c_i] = c->points[j];
            c_i++;
        }
    }

    c->num_points = c->num_points - 1;
    pfree( c->points );
    c->points = temp_points;

    // Erase hole reference if present
    for( j = 0; j < c->num_holes; j++ )
    {
        if( c->holes[j] == i )
        {
            temp_holes = ( unsigned int * ) palloc0(
                sizeof( unsigned int ) * ( c->num_holes - 1 )
            );

            if( temp_holes == NULL )
            {
                ereport(
                    ERROR,
                    (
                        errcode( ERRCODE_OUT_OF_MEMORY ),
                        errmsg( "Failed to resize hole map for contour" )
                    )
                );
            }

            for( h = 0; h < c->num_holes; h++ )
            {
                if( h != j )
                {
                    temp_holes[h] = c->holes[h];
                }
            }

            pfree( c->holes );
            c->holes     = temp_holes;
            c->num_holes = c->num_holes - 1;
        }
    }

    return;
}

/*
 * void contour_add_hole( struct contour *, unsigned int )
 *     Adds a hole of specified index to the contour
 *
 * Arguments:
 *     sturct contour * c: The contour being modified
 *     unsigned int i:     Index of the hole being added
 * Return:
 *     None
 * Error conditions:
 *     Error on failure to reallocate holes[] array
 */
void contour_add_hole( struct contour * c, unsigned int index )
{
    if( c == NULL )
    {
        return;
    }

    if( c->holes == NULL )
    {
        if( c->num_holes != 0 )
        {
            return;
        }

        c->holes = ( unsigned int * ) palloc0( sizeof( unsigned int ) );

        if( c->holes == NULL )
        {
            ereport(
                ERROR,
                (
                    errcode( ERRCODE_OUT_OF_MEMORY ),
                    errmsg( "Failed to create contour holes map" )
                )
            );
        }

        c->num_holes = 1;
        c->holes[0] = index;
    }
    else
    {
        c->holes = ( unsigned int * ) repalloc(
            c->holes,
            sizeof( unsigned int ) * ( c->num_holes + 1 )
        );

        if( c->holes == NULL )
        {
            ereport(
                ERROR,
                (
                    errcode( ERRCODE_OUT_OF_MEMORY ),
                    errmsg( "Failed to resize contour hole map" )
                )
            );
        }

        c->holes[c->num_holes] = index;
        c->num_holes = c->num_holes + 1;
    }

    return;
}

void contour_set_external( struct contour * c, bool ext )
{
    if( c == NULL )
    {
        return;
    }

    c->_external = ext;

    return;
}

void contour_add_point( struct contour * c, Point * p )
{
    if( c == NULL )
    {
        return;
    }

    if( c->points == NULL )
    {
        if( c->num_points != 0 )
        {
            return;
        }

        c->points = ( Point ** ) palloc0( sizeof( Point * ) );

        if( c->points == NULL )
        {
            ereport(
                ERROR,
                (
                    errcode( ERRCODE_OUT_OF_MEMORY ),
                    errmsg( "Could not create contour point array" )
                )
            );
        }

        c->num_points = 1;
        c->points[0]  = p;
    }
    else
    {
        c->points = ( Point ** ) repalloc(
            c->points,
            sizeof( Point * ) * ( c->num_points + 1 )
        );

        if( c->points == NULL )
        {
            ereport(
                ERROR,
                (
                    errcode( ERRCODE_OUT_OF_MEMORY ),
                    errmsg( "Could not resize contour point array" )
                )
            );
        }

        c->points[c->num_points] = p;
        c->num_points            = c->num_points + 1;
    }

    return;
}

struct segment * contour_get_segment( struct contour * c, unsigned int index )
{
    struct segment * s = NULL;

    if( c == NULL )
    {
        return NULL;
    }

    s = new_segment();

    if( index == ( c->num_points - 1 ) )
    {
        segment_set_begin( s, c->points[c->num_points - 1] );
        segment_set_end( s, c->points[0] );
    }
    else
    {
        segment_set_begin( s, c->points[index] );
        segment_set_end( s, c->points[index + 1] );
    }

    return s;
}

struct contour * new_contour( void )
{
    struct contour * c = NULL;

    c = ( struct contour * ) palloc0( sizeof( struct contour ) );

    if( c == NULL )
    {
        ereport(
            ERROR,
            (
                errcode( ERRCODE_OUT_OF_MEMORY ),
                errmsg( "Failed to create new contour" )
            )
        );
    }

    c->num_points      = 0;
    c->num_holes       = 0;
    c->_external       = false;
    c->_precomputed_cc = false;
    c->_cc             = false;

    return c;
}

void free_contour( struct contour * c )
{
    if( c == NULL )
    {
        return;
    }

    if( c->points != NULL )
    {
        pfree( c->points );
    }

    if( c->holes != NULL )
    {
        pfree( c->holes );
    }

    pfree( c );

    return;
}

double contour_area( struct contour * c )
{
    double       area = 0.0;
    unsigned int i    = 0;

    if( c == NULL )
    {
        return area;
    }

    for( i = 0; i < c->num_points; i++ )
    {
        if( i == c->num_points - 1 )
        {
            area += c->points[i]->x * c->points[0]->y
                  - c->points[0]->x * c->points[i]->y;
        }
        else
        {
            area += c->points[i]->x * c->points[i + 1]->y
                  - c->points[i + 1]->x * c->points[i]->y;
        }
    }

    area = area / 2;
    return area;
}

#ifdef DEBUG
void _dump_contour( struct contour * c )
{
    unsigned int i = 0;

    elog( DEBUG1, "Dumping contour %p", c );

    if( c == NULL )
    {
        return;
    }

    elog(
        DEBUG1,
        "points: %p\nnum_points: %d\nholes: %p\nnum_holes: %d\n"\
        "_external: %s\n_precompiled_cc: %s\n_cc: %s",
        c->points,
        c->num_points,
        c->holes,
        c->num_holes,
        c->_external ? "true" : "false",
        c->_precomputed_cc ? "true" : "false",
        c->_cc ? "true" : "false"
    );

    for( i = 0; i < c->num_points; i++ )
    {
        if( c->points[i] == NULL )
        {
            elog( DEBUG1, "points[%d]: NULL", i );
        }
        else
        {
            elog(
                DEBUG1,
                "points[%d]: (%f,%f) ADDR %p",
                i,
                c->points[i]->x,
                c->points[i]->y,
                c->points[i]
            );
        }
    }

    for( i = 0; i < c->num_holes; i++ )
    {
        elog( DEBUG1, "holes[%d]: %d", i, c->holes[i] );
    }

    return;
}
#endif // DEBUG
